import pickle

import redis

#redis-server

def connect():
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    return r


def set_by_key(key, value, r=connect()):
    pickled_object = pickle.dumps(value)
    r.set(key, pickled_object)

def get_by_key(key, r=connect()):
    return pickle.loads(r.get(key))
