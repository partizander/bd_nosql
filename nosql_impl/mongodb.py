# https://docs.mongodb.com/getting-started/python/query/

from pymongo import MongoClient
import pickle

from datetime import datetime


def connect():
    client = MongoClient()
    db = client.test
    return db


def set_by_key(key, value, db=connect()):
    pickled_object = pickle.dumps(value)
    db.restaurants.insert_one({"id":key, "data":pickled_object})


def get_by_key(key, db=connect()):
    print (key)
    cursor = db.test.find({"id":key})

    for i in cursor:
        print (i)
