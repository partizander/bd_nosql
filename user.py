from utils import get_by_key, set_by_key


def get_last_id():
    try:
        last_id = get_by_key("last_id")
        last_id = int(last_id)
    except:
        last_id = 0

    set_by_key("last_id", str(last_id + 1))
    return last_id


def set_new_user_in_bd(id):
    try:
        all_ids = get_by_key("all_users_id")
        all_ids.append(id)
    except:
        all_ids = [id]

    set_by_key("all_users_id", all_ids)


class User:
    def __init__(self, name):
        self.id = get_last_id() + 1
        self.name = name
        self.bought_sum = 0
        self.discount = self.bought_sum / 100 * 10
        self.products_bought = []

        set_new_user_in_bd(self.id)
        set_by_key("User_" + str(self.id), self)

    def do_buy(self, product_id, shop_id):
        product = get_by_key("Product_" + product_id)

        if product.is_discount:
            self.bought_sum += product.cost - (product.cost / 100 * self.discount)

        self.products_bought.append([product_id, shop_id])

        set_by_key("User_" + str(self.id), self)
