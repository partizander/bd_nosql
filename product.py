from utils import set_by_key


class Product:
    def __init__(self, id, name, cat, cost, is_discount):
        self.id = id
        self.name = name
        self.cat = cat
        self.cost = cost
        self.is_discount = is_discount

        set_by_key("Product_" + str(self.id), self)

    def __str__(self):
        return (str(self.id) + " " + self.name + " " + self.cat + " " + str(self.cost) + " " + str(self.is_discount))
