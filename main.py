from product import Product
from shop import Shop
from user import User
from utils import get_by_key


def create_users():
    user1 = User("Ivan")
    user2 = User("Stepan")
    user3 = User("Sergey")
    user4 = User("Alexander")
    user5 = User("Alexey")


def create_goods():
    product1 = Product("123", "macbook", "electronics", 32, True)
    product2 = Product("1234", "notepad", "stationery", 45, False)
    product3 = Product("12345", "pencil", "stationery", 16, True)
    product4 = Product("123456", "papers", "stationery", 23, False)
    product5 = Product("1234567", "eraser", "stationery", 20, True)


def create_shops():
    shop1 = Shop("12", "Moscow")
    shop2 = Shop("13", "Vladivostok")
    shop3 = Shop("14", "Saint Petersburg")


def do_buy_by_users():
    user1 = get_by_key("User_1")
    user2 = get_by_key("User_2")
    user3 = get_by_key("User_3")
    user4 = get_by_key("User_4")
    user5 = get_by_key("User_5")

    user1.do_buy("123", "12")
    user1.do_buy("123", "12")
    user1.do_buy("12345", "13")
    user1.do_buy("123456", "12")
    user1.do_buy("123456", "14")

    user4.do_buy("123", "14")
    user4.do_buy("12345", "12")
    user4.do_buy("12345", "13")
    user4.do_buy("123", "14")
    user4.do_buy("1234567", "12")


def weekly_report():
    all_ids = get_by_key("all_users_id")

    products_dict = {}

    for i in all_ids:
        user = get_by_key("User_" + str(i))
        products_bought_arr = user.products_bought

        for j in products_bought_arr:
            if not (j[0] in products_dict):
                products_dict[j[0]] = [j[1]]
            else:
                products_dict[j[0]].append(j[1])

    for i in products_dict:
        product_item = get_by_key("Product_" + str(i))
        print ("Product ")
        print (product_item)
        print ("was buy in ")

        for j in products_dict[i]:
            print (get_by_key("Shop_" + str(j)))

        print ("count " + str(len(products_dict[i])))
        print (" ")


create_users()

create_goods()

create_shops()

do_buy_by_users()

weekly_report()
