from utils import set_by_key


class Shop:
    def __init__(self, id, address):
        self.id = id
        self.address = address

        set_by_key("Shop_" + str(self.id), self)

    def __str__(self):
        return (str(self.id) + " " + self.address)
